const { Blog } = require("../models");
const Sequelize = require('sequelize')
const Op = Sequelize.Op;
// Create and Save a new blog
exports.create = async (req, res) => {
  try {
    await Blog.create(req.body)
    res.status(200).json("success");     
  } catch (error) {
    res.status(500).send({
      message:
        err.message || "Some error occurred while create blogs."
    });
  }    
};

// Retrieve all blogs from the database.
exports.findAll = async (req, res) => {
      try {
        const title = req.query.title;
        const condition = title ? { title: { [Op.like]: `%${title}%` } } : null;
        const blogs = await Blog.findAll({ where: condition })

        res.status(200).json(blogs);
      } catch (error) {
        res.status(500).send({
            message:
              err.message || "Some error occurred while retrieving blogs."
          });
      }
};
// Find a single blog with an id
exports.findOne = async (req, res) => {    
    try {
      const id = req.params.id;

      const blog = await Blog.findByPk(id)

      res.status(200).json(blog);         
    } catch (error) {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving blog."
      });
    }
    
};

// Update a blog by the id in the request
exports.update = async (req, res) => {
  try {
    const id = req.params.id;
    await Blog.update(req.body, {
      where: { id: id }
    });
    res.status(200).json("success");     
  } catch (error) {
    res.status(500).send({
      message:
        err.message || "Some error occurred while update blog."
    });
  }    
};

// Delete a blog with the specified id in the request
exports.delete = async (req, res) => {
  try {
    const id = req.params.id;
    await Blog.destroy({ where: { id: id } });
    res.status(200).json("success");     
  } catch (error) {
    res.status(500).send({
      message:
        err.message || "Some error occurred while delete blog."
    });
  }      
};

// Delete all blogs from the database.
exports.deleteAll = (req, res) => {
  
};
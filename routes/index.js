module.exports = app => {
  const blog = require("../controllers/index.controller.js");

  var router = require("express").Router();

  // Create a new blog
  router.post("/create/", blog.create);

  // Retrieve all blog
  router.get("/list/", blog.findAll);

  // Retrieve a single blog with id
  router.get("/list/:id", blog.findOne);

  // Update a blog with id
  router.post("/update/:id", blog.update);

  // Delete a blog with id
  router.post("/delete/:id", blog.delete);

  // Delete all blog
  router.post("/deleteAll", blog.deleteAll);

  app.use('/api/blog', router);
};